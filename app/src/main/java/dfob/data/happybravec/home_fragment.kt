package dfob.data.happybravec

import android.net.wifi.hotspot2.pps.HomeSp
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

class home_fragment : Fragment(R.layout.home_fragment)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        val startButton = view.findViewById<Button>(R.id.StartButton_VHF)
        val action = home_fragmentDirections.actionHomeFragmentToSecondFragment()
        startButton.setOnClickListener{
            findNavController().navigate(action)
        }

        // For the quick fix button.
        val quickFixButton = view.findViewById<Button>(R.id.QuickFixButton_VHF)
        val actionB = home_fragmentDirections.actionHomeFragmentToFifthFragment()
        quickFixButton.setOnClickListener{
            findNavController().navigate(actionB)
        }
    }

}