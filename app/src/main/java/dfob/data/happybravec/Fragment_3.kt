package dfob.data.happybravec

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController

class Fragment_3 : Fragment(R.layout.fragment_3)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        val nextButtonV3 = view.findViewById<Button>(R.id.NextButton_V3)
        val cb1 = view.findViewById<CheckBox>(R.id.checkBoxExcitedV3)
        val cb2 = view.findViewById<CheckBox>(R.id.checkBoxGoodV3)
        val cb3 = view.findViewById<CheckBox>(R.id.checkBoxRelaxedV3)

        // Action to pass to the other fragment.
        nextButtonV3.setOnClickListener{view.findNavController().navigate(R.id.action_ThirdFragment_to_FourthFragment)}

        //Allow the user to just choose one check box.
        cb1.setOnClickListener{
            cb2.isChecked = false
            cb3.isChecked = false
        }

        cb2.setOnClickListener{
            cb1.isChecked = false
            cb3.isChecked = false
        }

        cb3.setOnClickListener{
            cb2.isChecked = false
            cb1.isChecked = false
        }
    }
}