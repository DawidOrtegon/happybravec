package dfob.data.happybravec

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController

class Fragment_4 : Fragment(R.layout.fragment_4)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        val nextButtonV4 = view.findViewById<Button>(R.id.NextButton_V4)
        val cb1 = view.findViewById<CheckBox>(R.id.yellowcheckBox)
        val cb2 = view.findViewById<CheckBox>(R.id.redcheckBox)
        val cb3 = view.findViewById<CheckBox>(R.id.bluecheckBox)
        val cb4 = view.findViewById<CheckBox>(R.id.greencheckBox)
        val cb5 = view.findViewById<CheckBox>(R.id.purplecheckBox)
        val cb6 = view.findViewById<CheckBox>(R.id.orangecheckBox)

        // Call the other action to pass to the other fragment.
        nextButtonV4.setOnClickListener{view.findNavController().navigate(R.id.action_FourthFragment_to_FifthFragment)}

        // Allow the user to check only one check box.
        cb1.setOnClickListener{
            cb2.isChecked = false
            cb3.isChecked = false
            cb4.isChecked = false
            cb5.isChecked = false
            cb6.isChecked = false
        }
        cb2.setOnClickListener{
            cb1.isChecked = false
            cb3.isChecked = false
            cb4.isChecked = false
            cb5.isChecked = false
            cb6.isChecked = false
        }
        cb3.setOnClickListener{
            cb1.isChecked = false
            cb2.isChecked = false
            cb4.isChecked = false
            cb5.isChecked = false
            cb6.isChecked = false
        }
        cb4.setOnClickListener{
            cb1.isChecked = false
            cb2.isChecked = false
            cb3.isChecked = false
            cb5.isChecked = false
            cb6.isChecked = false
        }
        cb5.setOnClickListener{
            cb1.isChecked = false
            cb2.isChecked = false
            cb3.isChecked = false
            cb4.isChecked = false
            cb6.isChecked = false
        }
        cb6.setOnClickListener{
            cb1.isChecked = false
            cb2.isChecked = false
            cb3.isChecked = false
            cb4.isChecked = false
            cb5.isChecked = false

        }
    }
}