package dfob.data.happybravec

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController

class Fragment_2 : Fragment(R.layout.fragment_2)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        val nextButtonV2 = view.findViewById<Button>(R.id.NextButton_V2)
        val cb1 = view.findViewById<CheckBox>(R.id.checkBoxReallySadV2)
        val cb2 = view.findViewById<CheckBox>(R.id.worrycheckBoxV2)

        // Call the action to pass to the other fragment.
        nextButtonV2.setOnClickListener{view.findNavController().navigate(R.id.action_SecondFragment_to_ThirdFragment)}

        // Allow the user to just choose one check box.
        cb1.setOnClickListener {
            cb2.isChecked = false
        }
        cb2.setOnClickListener{
            cb1.isChecked = false
        }
    }

}