package dfob.data.happybravec

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.fragment.app.Fragment

class Fragment_5 : Fragment(R.layout.fragment_5) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        val cb1 = view.findViewById<CheckBox>(R.id.musiccheckBox)
        val cb2 = view.findViewById<CheckBox>(R.id.seriescheckBox)
        val cb3 = view.findViewById<CheckBox>(R.id.foodcheckBox)
        val cb4 = view.findViewById<CheckBox>(R.id.mhelpcheckbox)

        // Allow the user to only check one option.
        cb1.setOnClickListener{
            cb2.isChecked = false
            cb3.isChecked = false
            cb4.isChecked = false
        }
        cb2.setOnClickListener{
            cb1.isChecked = false
            cb3.isChecked = false
            cb4.isChecked = false
        }
        cb3.setOnClickListener{
            cb1.isChecked = false
            cb2.isChecked = false
            cb4.isChecked = false
        }
        cb4.setOnClickListener{
            cb1.isChecked = false
            cb2.isChecked = false
            cb3.isChecked = false
        }
    }
}